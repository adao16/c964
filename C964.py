import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn import metrics

url = "https://gitlab.com/adao16/c964/-/raw/main/Obesity_Data.csv"
df = pd.read_csv(url)

""" Visualizations of dataset
num_types = df.groupby(by=('overweight_status')).size();
display(num_types);

plot = num_types.plot.bar(color=['red','blue'], rot = 0)
plt.show()

hist_weight = df['weight'].hist(grid = False,bins = 10, legend = True)
plt.show()

lmplot = sns.lmplot(x='weight',y='height', data=df, fit_reg=False, hue='overweight_status')
plt.show()
"""

numarray = df.to_numpy()
X = numarray[:,0:5]
y = numarray[:,5]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.333, random_state=41)

svm_model = svm.SVC(gamma='scale', C=1)
svm_model.fit(X_train,y_train)

predictions = svm_model.predict(X_train) 
score = metrics.accuracy_score(y_train, predictions)

predictions_test = svm_model.predict(X_test)
score2 = metrics.accuracy_score(y_test, predictions_test)

